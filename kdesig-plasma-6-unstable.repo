[copr:copr.fedorainfracloud.org:group_kdesig:plasma-6-unstable]
name=Copr repo for plasma-6-unstable owned by @kdesig
baseurl=https://download.copr.fedorainfracloud.org/results/@kdesig/plasma-6-unstable/fedora-rawhide-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://download.copr.fedorainfracloud.org/results/@kdesig/plasma-6-unstable/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1

# Explicitely exclude all KDE packages that must come from the nightly repo
# from the Fedora repos to make sure we get them from the nightly copr
# Generated from:
# $ podman run --rm -ti fedora:rawhide
# $ dnf install -y 'dnf-command(copr)' && dnf copr enable -y @kdesig/plasma-6-unstable
# $ dnf repository-packages "copr:copr.fedorainfracloud.org:group_kdesig:plasma-6-unstable" list \
#   | grep "copr:copr.fedorainfracloud.org:group_kdesig:plasma-6-unstable" | grep -vE "(debug|devel|\.src)" \
#   | cut -f1 -d\ | sed 's/\.x86_64//' | sed 's/\.noarch//' | paste -sd " " -

[fedora-rawhide-latest]
name=Fedora rawhide $basearch (latest)
baseurl=http://kojipkgs.fedoraproject.org/repos/rawhide/latest/$basearch/
enabled=1
gpgcheck=0
metadata_expire=1d
excludepkgs=arianna bluedevil bomber bovo breeze-cursor-theme breeze-gtk breeze-gtk-common breeze-gtk-gtk2 breeze-gtk-gtk3 breeze-gtk-gtk4 elisa-player filelight flatpak-kcm granatier grub2-breeze-theme kactivities kactivities-stats kactivitymanagerd kalk kapman katomic kblackbox kblocks kbounce kbrickbuster kde-cli-tools kde-gtk-config kde-inotify-survey kdecoration kdeedu-data kdeplasma-addons kdesu kdsoap kdsoap-doc kdsoap-ws-discovery-client kdsoap6 kf5-kglobalaccel kf5-kglobalaccel-libs kinfocenter kio-extras kio-extras-info kio-extras-kf6 kio-extras-kf6-info kmahjongg kmenuedit kmines kpat kpipewire krfb krfb-libs kruler kscreen kscreenlocker ksshaskpass ksystemstats kwayland kwayland-integration kwin kwin-common kwin-doc kwin-libs kwin-wayland kwin-x11 kwordquiz kwrited layer-shell-qt libkdegames libkeduvocdocument libkexiv2-qt5 libkexiv2-qt6 libkmahjongg libkmahjongg-data libkscreen libksysguard libksysguard-common libkworkspace5 libkworkspace6 oxygen-cursor-themes oxygen-sounds pam-kwallet phonon-common phonon-qt5 phonon-qt6 plasma-breeze plasma-breeze-common plasma-breeze-qt5 plasma-breeze-qt6 plasma-desktop plasma-desktop-doc plasma-desktop-kimpanel-scim plasma-discover plasma-discover-flatpak plasma-discover-libs plasma-discover-notifier plasma-discover-offline-updates plasma-discover-packagekit plasma-discover-rpm-ostree plasma-disks plasma-drkonqi plasma-firewall plasma-firewall-firewalld plasma-firewall-ufw plasma-framework plasma-integration plasma-integration-qt5 plasma-lookandfeel-fedora plasma-milou plasma-nano plasma-nm plasma-nm-fortisslvpn plasma-nm-iodine plasma-nm-l2tp plasma-nm-openconnect plasma-nm-openswan plasma-nm-openvpn plasma-nm-pptp plasma-nm-ssh plasma-nm-sstp plasma-nm-strongswan plasma-nm-vpnc plasma-oxygen plasma-oxygen-qt5 plasma-oxygen-qt6 plasma-pa plasma-print-manager plasma-print-manager-libs plasma-sdk plasma-systemmonitor plasma-systemsettings plasma-thunderbolt plasma-vault plasma-wayland-protocols plasma-welcome plasma-workspace plasma-workspace-common plasma-workspace-doc plasma-workspace-geolocation plasma-workspace-geolocation-libs plasma-workspace-libs plasma-workspace-wallpapers plasma-workspace-wayland plasma-workspace-x11 plasma5support plymouth-kcm plymouth-theme-breeze polkit-kde powerdevil qaccessibilityclient sddm sddm-breeze sddm-kcm sddm-themes sddm-wayland-generic sddm-wayland-plasma sddm-x11 spectacle xdg-desktop-portal-kde xdg-utils xwaylandvideobridge

[fedora-rawhide-nokde]
name=Fedora rawhide $basearch
mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=rawhide&arch=$basearch
enabled=1
gpgcheck=1
metadata_expire=1d
excludepkgs=arianna bluedevil bomber bovo breeze-cursor-theme breeze-gtk breeze-gtk-common breeze-gtk-gtk2 breeze-gtk-gtk3 breeze-gtk-gtk4 elisa-player filelight flatpak-kcm granatier grub2-breeze-theme kactivities kactivities-stats kactivitymanagerd kalk kapman katomic kblackbox kblocks kbounce kbrickbuster kde-cli-tools kde-gtk-config kde-inotify-survey kdecoration kdeedu-data kdeplasma-addons kdesu kdsoap kdsoap-doc kdsoap-ws-discovery-client kdsoap6 kf5-kglobalaccel kf5-kglobalaccel-libs kinfocenter kio-extras kio-extras-info kio-extras-kf6 kio-extras-kf6-info kmahjongg kmenuedit kmines kpat kpipewire krfb krfb-libs kruler kscreen kscreenlocker ksshaskpass ksystemstats kwayland kwayland-integration kwin kwin-common kwin-doc kwin-libs kwin-wayland kwin-x11 kwordquiz kwrited layer-shell-qt libkdegames libkeduvocdocument libkexiv2-qt5 libkexiv2-qt6 libkmahjongg libkmahjongg-data libkscreen libksysguard libksysguard-common libkworkspace5 libkworkspace6 oxygen-cursor-themes oxygen-sounds pam-kwallet phonon-common phonon-qt5 phonon-qt6 plasma-breeze plasma-breeze-common plasma-breeze-qt5 plasma-breeze-qt6 plasma-desktop plasma-desktop-doc plasma-desktop-kimpanel-scim plasma-discover plasma-discover-flatpak plasma-discover-libs plasma-discover-notifier plasma-discover-offline-updates plasma-discover-packagekit plasma-discover-rpm-ostree plasma-disks plasma-drkonqi plasma-firewall plasma-firewall-firewalld plasma-firewall-ufw plasma-framework plasma-integration plasma-integration-qt5 plasma-lookandfeel-fedora plasma-milou plasma-nano plasma-nm plasma-nm-fortisslvpn plasma-nm-iodine plasma-nm-l2tp plasma-nm-openconnect plasma-nm-openswan plasma-nm-openvpn plasma-nm-pptp plasma-nm-ssh plasma-nm-sstp plasma-nm-strongswan plasma-nm-vpnc plasma-oxygen plasma-oxygen-qt5 plasma-oxygen-qt6 plasma-pa plasma-print-manager plasma-print-manager-libs plasma-sdk plasma-systemmonitor plasma-systemsettings plasma-thunderbolt plasma-vault plasma-wayland-protocols plasma-welcome plasma-workspace plasma-workspace-common plasma-workspace-doc plasma-workspace-geolocation plasma-workspace-geolocation-libs plasma-workspace-libs plasma-workspace-wallpapers plasma-workspace-wayland plasma-workspace-x11 plasma5support plymouth-kcm plymouth-theme-breeze polkit-kde powerdevil qaccessibilityclient sddm sddm-breeze sddm-kcm sddm-themes sddm-wayland-generic sddm-wayland-plasma sddm-x11 spectacle xdg-desktop-portal-kde xdg-utils xwaylandvideobridge
